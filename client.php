<?php
use GuzzleHttp\Client;

require_once 'vendor/autoload.php';

define('SERVER_URL', 'http://localhost:8000/');
define('STATIONS_FILE_PATH', __DIR__ . '/data/stations.dat');
define('CONFIG_FILE_PATH', __DIR__ . '/data/ssa.json');
define('STATIONS_SPLITED_BASE_PATH', __DIR__ . '/data/sample-logs-splited');
define('SPLITS_PER_STATION', 3);



//You do not need to change anything after this line

if (!file_exists(STATIONS_FILE_PATH)) {
    echo "Could not find stations file. Aborting." . PHP_EOL;
    exit;
}

if (!file_exists(CONFIG_FILE_PATH)) {
    echo "Could not find config file. Aborting." . PHP_EOL;
    exit;
}




$baseUri = preg_match('#/$#', SERVER_URL) ? SERVER_URL : SERVER_URL . '/';

$client = new Client([
    'base_uri' => $baseUri,
    'timeout'  => 60
]);

//creating a session
$sessionUuid = createSession($client, STATIONS_FILE_PATH, CONFIG_FILE_PATH);
message('Session created. UUID: ' . $sessionUuid . '.');



//creating spans (1 for each split)
message('Creating spans...');
$spans = [];
for ($i = 0; $i < SPLITS_PER_STATION; ++$i) {
    $spans[] = createSpan($client, $sessionUuid);
    message('Span created. UUID: ' . $spans[$i]);
}
message('Done.');

message('Gathering files...');
//gathering all files to shuffle and send them to server
$stationFolders = glob(STATIONS_SPLITED_BASE_PATH . '/*');

$files = [];
foreach ($stationFolders as $folder) {
    $station      = basename($folder);
    $stationFiles = glob($folder . '/*');

    foreach ($stationFiles as $file) {
        $span = basename($file);
        $files[] = [
            'filename' => $file,
            'endpoint' => 'api/sessions/' . $sessionUuid . '/spans/' . $spans[$span] . '/logs',
            'field'    => 'station_' . $station
        ];
    }
}

srand((float)microtime()*1000000);
shuffle($files);

message('Done.');
message('Start sending...');

$times     = 0;
$sendCount = count($files);
foreach ($files as $file) {
    $times++;
    message('Sending data to server (' . $times . '/' . $sendCount . ')...');
    $response = (array) convertResponseToJson(sendLogs($client, $file), 'Error reading server response');
    message('Done (' . $times . ').');
}
message('Done sending data.');
message('Starting monitoring data processing...');
startMonitoring($client, $sessionUuid);
message('--DONE--');
