<?php
require_once "vendor/autoload.php";


function convertResponseToJson($response, $errorMessage)
{
    $info = json_decode($response->getBody());

    if (!$info) {
        echo $errorMessage . PHP_EOL;
        exit;
    }

    return $info;
}

function createSession($client, $stationsPath, $configPath)
{
    $sessionResponse = $client->request(
        'POST',
        '/api/sessions',
        [
            'multipart' => [
                [
                    'name' => 'name',
                    'contents' => 'Sample Session'
                ],
                [
                    'name' => 'config',
                    'filename' => basename($configPath),
                    'contents' => file_get_contents($configPath)
                ],
                [
                    'name' => 'stations',
                    'filename' => basename($stationsPath),
                    'contents' => file_get_contents($stationsPath)
                ]
            ]
        ]
    );

    $info = convertResponseToJson($sessionResponse, "Couldn't create a session. Aborting. Is the server running?");

    return $info->session->uuid;
}

function createSpan($client, $sessionUuid)
{
    $spanResponse = $client->request(
        'POST',
        '/api/sessions/' . $sessionUuid . '/spans'
    );
    $info = convertResponseToJson($spanResponse, "Couldn't create a span. Aborting. Is the server running?");
    return $info->span->uuid;
}

function sendLogs($client, $logInfo)
{
    $multipart = [];
    /* foreach ($logInfos as $logInfo) { */
        $multipart[] = [
            'name'     => $logInfo['field'],
            'contents' => file_get_contents($logInfo['filename']),
            'filename' => basename($logInfo['filename'])
        ];
    /* } */
    return $client->request(
        'POST',
        $logInfo['endpoint'],
        ['multipart' => $multipart]
    );
}

function startMonitoring($client, $sessionUuid)
{
    message('Fetching the server...');
    $spans = (array) convertResponseToJson(
        $client->request(
            'GET',
            'api/sessions/' . $sessionUuid . '/spans'
        ),
        'Error reading info from the server.'
    );
    message('Done.');
    message('----');

    $count = count($spans);
    $done  = 0;
    foreach ($spans as $span) {
        message('Span ' . $span->uuid . ": \t" . $span->status);
        if ($span->status == 'done') {
            $done++;
        }
    }
    message('----');
    if ($done < $count) {
        message("Processing not done yet...");
        message('----');
        sleep(1);
        startMonitoring($client, $sessionUuid);
    } else {
        showResults($client, $spans);
    }
}

function showResults($client, $spans)
{
    foreach ($spans as $span) {
        $response = $client->request('GET', $span->result);
        message('----' . $span->uuid . ' RESULT ----');
        message((string) $response->getBody());
        message('----' . str_repeat('-', strlen($span->uuid . ' RESULT ')) . '---');
        message("\n");
    }
}

function message($message)
{
    echo $message . PHP_EOL;
}
