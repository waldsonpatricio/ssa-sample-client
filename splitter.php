<?php
$sourceDir = __DIR__ . '/data/sample-logs';
$destDir   = __DIR__ . '/data/sample-logs-splited';
$numberOfLines  = 120000;
$numberOfSplits = 5;

$linesPerSplit = $numberOfLines / $numberOfSplits;

$files = glob($sourceDir . '/*.dat');



foreach ($files as $file) {
    $handle = fopen($file, 'r');

    $station = preg_replace('#raw.dat#', '', basename($file));

    if (!is_dir($destDir . '/' . $station)) {
        mkdir($destDir . '/' . $station, 0755, true);
    }

    $currentLine = 0;
    $currentFile = -1;
    $splitHandle = null;


    while (!feof($handle)) {
        $newFile = intval($currentLine / $linesPerSplit);
        if ($currentLine == $numberOfLines) {
            fclose($splitHandle);
            break;
        }

        if ($newFile != $currentFile) {
            $currentFile = $newFile;
            if (!empty($splitHandle)) {
                fclose($splitHandle);
            }
            $splitHandle = fopen($destDir . '/' . $station . '/' . $currentFile, 'w');
        }

        $line = fgets($handle);
        fwrite($splitHandle, $line);

        $currentLine++;
    }


    fclose($handle);
}
