# Requirements

- PHP 7.1+ with curl extension enabled
- Composer

# Running

- Run ```composer install```
- Run ```composer dump-autoload```
- Extract ```data/sample-logs-splited.zip``` to ```data/sample-logs-splited``` folder
- Open ```client.php``` file and set the SERVER_URL constant with your server URL
- Run ```php client.php```
